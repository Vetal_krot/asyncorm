# -*-coding:utf-8-*-
from twisted.trial import unittest
from asyncorm.backends.postgresql.compiler import PGCompiler
from asyncorm.expressions import AS, Q, Procedure, COUNT
from asyncorm.query import Select, Insert, Update, Delete
from asyncorm import models


class C(object):
    formatter = '%s'

    def get_expression_pattern(self, *args):
        return


class Model1(models.Model):
    class Meta:
        db_table = 'model1'
        ordering = ['-id', 'field1']

    field1 = models.CharField(max_length=80)


class Model2(models.Model):
    class Meta:
        db_table = 'model2'

    field1 = models.CharField(max_length=12)


class MyProcedure(Procedure):
    procedure_name = 'hard_func'


class FakeBackend(object):
    compiler = PGCompiler()

    def db_select(self, sql, params):
        print 'executing...', sql, params

    db_insert = db_update = db_delete = db_select

    def get_compiler(self):
        return self.compiler


class TestSelectQuery(unittest.TestCase):

    def setUp(self):
        self.fields = ['field1', 'field2', COUNT('table_name')]
        self.db_table = 'table_name'

    def test_create(self):
        q = Select(Model1.id, Model1.field1, AS(Select('id').from_table('sub_table').where('a = %s', [2])[1], 'sub_q')).from_model(Model1).where('field1 = %s', [1])[1:3]
        q.distinct(Model1.id)
        q.where('field4 IN %s', [(1, 2, 310)])
        q.filter((Model1.pk + 'fff') != 4,
            field1__not_equals=Select('id').from_table('some_table').where('a = {}', [100])[1],
            id__iendswith=3, field1__contains=300)
        q.join('table2', Q(Model1.id == 12, Model1.field1.not_between(10, 20))
               | Q(Model1.field1.in_list([1, 2, 3]), Model2.field1 == 3))
        q.left_join(Model2, Model1.id == Model2.id, 'dd=ff')
        # q.order_by('-field1', MyProcedure(1, 'DSF'), 'id')
        # q.returning('id', 'field1', MyProcedure(1, 'DSF'))
        q.for_update(True)
        q.execute(FakeBackend())

    def test_insert(self):
        q = Insert('field1', 'field2').into('table_name').values({'field1': 1, 'field2': 2}, {'field1': 12, 'field2': 22}).values(field2=3).returning('id')
        q.execute(FakeBackend())
        q = Insert().into(Model1).from_select(Select().from_model(Model2).where('id IN %s', [(1, 2, 3)])[1]).returning('id')
        q.execute(FakeBackend())

    def test_update(self):
        q = Update(Model1).set(field1=Model1.field1 + 1).filter(id=1).returning('id')
        q.execute(FakeBackend())

    def test_delete(self):
        q = Delete(Model1).where('some_filed IN %s', [(1, 2, 3)]).filter(field1__contains=2).returning('id')
        q.filter(id=Select('id').from_procedure(MyProcedure(1, 2, 'sads'))[1])
        q.execute(FakeBackend())


__author__ = 'Vetal_krot'
