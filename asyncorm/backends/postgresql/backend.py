from psycopg2.extensions import register_adapter, Binary, AsIs
from shapely import geometry
from aiopg.pool import _create_pool
from asyncorm.backends.postgresql.compiler import PGCompiler
from asyncorm.backends.backend import BackendBase, BackendFeatures
from asyncorm.backends.postgresql.transaction import PGTransaction


def gis_adaptor(value):
    return AsIs('ST_GeomFromWKB({0}, {1})'.format(Binary(value.wkb), 4326))


register_adapter(geometry.GeometryCollection, gis_adaptor)
register_adapter(geometry.Point, gis_adaptor)
register_adapter(geometry.LineString, gis_adaptor)
register_adapter(geometry.Polygon, gis_adaptor)
register_adapter(geometry.MultiPoint, gis_adaptor)
register_adapter(geometry.MultiLineString, gis_adaptor)
register_adapter(geometry.MultiPolygon, gis_adaptor)


class PostgresSQLBackend(BackendBase):
    transaction_factory = PGTransaction
    features = BackendFeatures(support_returning=True, support_for_update=True)
    compiler = PGCompiler()

    @property
    def dsn(self):
        dsn = {
            'dbname': self.database,
            'user': self.user,
            'password': self.password,
            'host': self.host
        }
        return ' '.join('='.join(opt) for opt in dsn.items())

    def create_connection_pool(self):
        return _create_pool(self.dsn, loop=self._loop, **self._conn_kwargs)

