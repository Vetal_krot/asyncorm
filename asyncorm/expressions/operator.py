from asyncorm import constants
from asyncorm.interfaces.node import INode
from asyncorm.interfaces.compiler import ICompilable
from asyncorm.interfaces.expression import IExpression
from asyncorm.expressions.structure import AS
from asyncorm.expressions.util import compile_iterable
from asyncorm.expressions.structure import JoinExpressions


@INode.register
class Node(object):
    def equals(self, other):
        if other is None:
            return Is(self, None)
        return Equals(self, other)

    def not_equals(self, other):
        if other is None:
            return IsNot(self, None)
        return NotEquals(self, other)

    def isnull(self, isnull):
        if isnull:
            return Is(self, None)
        return IsNot(self, None)

    def less(self, other):
        return Less(self, other)

    def less_or_equals(self, other):
        return LessOrEquals(self, other)

    def great(self, other):
        return Great(self, other)

    def great_or_equals(self, other):
        return GreatOrEquals(self, other)

    def in_list(self, other):
        return In(self, other)

    def not_in_list(self, other):
        return NotIn(self, other)

    def between(self, value1, value2=None):
        if isinstance(value1, (list, tuple)):
            value1, value2 = value1
        return Between(self, value1, value2)

    def not_between(self, value1, value2=None):
        if isinstance(value1, (list, tuple)):
            value1, value2 = value1
        return NotBetween(self, value1, value2)

    def like(self, other):
        return Like(self, other)

    def not_like(self, other):
        return NotLike(self, other)

    def ilike(self, other):
        return ILike(self, other)

    def not_ilike(self, other):
        return NotILike(self, other)

    def contains(self, other):
        return self.like('%{0}%'.format(other))

    def not_contains(self, other):
        return self.not_like('%{0}%'.format(other))

    def startswith(self, other):
        return self.like('{0}%'.format(other))

    def not_startswith(self, other):
        return self.not_like('{0}%'.format(other))

    def endswith(self, other):
        return self.like('%{0}'.format(other))

    def not_endswith(self, other):
        return self.not_ilike('%{0}'.format(other))

    def icontains(self, other):
        return self.ilike('%{0}%'.format(other))

    def not_icontains(self, other):
        return self.not_ilike('%{0}%'.format(other))

    def istartswith(self, other):
        return self.ilike('{0}%'.format(other))

    def not_istartswith(self, other):
        return self.not_ilike('{0}%'.format(other))

    def iendswith(self, other):
        return self.ilike('%{0}'.format(other))

    def not_iendswith(self, other):
        return self.not_ilike('%{0}'.format(other))

    def regex(self, other):
        return Regex(self, other)

    def iregex(self, other):
        return iRegex(self, other)

    def concat(self, other):
        if isinstance(other, list):
            return ConcatMany(self, *other)
        return Concat(self, other)

    def add(self, other):
        if isinstance(other, (int, float)):
            return Add(self, other)
        return self.concat(other)

    def sub(self, other):
        return Sub(self, other)

    def mul(self, other):
        return Mul(self, other)

    def div(self, other):
        return Div(self, other)

    def label(self, label):
        return AS(self, label)

    def __or__(self, other):
        return OR([self, other])

    def __and__(self, other):
        if isinstance(other, tuple):
            other = AND(other)
        return AND((self, other))

    __eq__ = eq = equals
    __ne__ = ne = not_equals
    __lt__ = lt = less
    __le__ = lte = less_or_equals
    __gt__ = gt = great
    __ge__ = gte = great_or_equals
    __add__ = add
    __sub__ = sub
    __mul__ = mul
    __div__ = div


@ICompilable.register
@IExpression.register
class RawExpression(object):
    def __init__(self, sql, params=None):
        self.sql = sql
        self.params = params

    def compile(self, backand):
        return self.sql, self.params


@ICompilable.register
@IExpression.register
class Expression(Node):
    expr = None
    default_pattern = None

    def __init__(self, value1, value2):
        self.value1 = value1
        self.value2 = value2

    def _get_expression_pattern(self, compiler):
        return compiler.get_expression(self.expr) or self.default_pattern

    def _get_values(self):
        return self.value1, self.value2

    def compile(self, compiler):
        sql_pattern = self._get_expression_pattern(compiler)
        sql, params = compile_iterable(self._get_values(), compiler)
        return sql_pattern.format(*sql), params


class LogicalJoinExpression(JoinExpressions, Node):
    delimiter = None

    def compile(self, compiler):
        sql, params = JoinExpressions.compile(self, compiler)
        if len(self.values) == 1:
            return sql, params
        return '({0})'.format(sql), params


class AND(LogicalJoinExpression):
    delimiter = ' AND '


Q = lambda *args: AND(args)


class OR(LogicalJoinExpression):
    delimiter = ' OR '


class Equals(Expression):
    expr = constants.EX_EQUALS
    default_pattern = '{0} = {1}'


class NotEquals(Expression):
    expr = constants.EX_NOT_EQUALS
    default_pattern = '{0} != {1}'


class Less(Expression):
    expr = constants.EX_LESS
    default_pattern = '{0} < {1}'


class LessOrEquals(Expression):
    expr = constants.EX_LESS_OR_EQUALS
    default_pattern = '{0} <= {1}'


class Great(Expression):
    expr = constants.EX_GREAT
    default_pattern = '{0} > {1}'


class GreatOrEquals(Expression):
    expr = constants.EX_GREAT
    default_pattern = '{0} >= {1}'


class Is(Expression):
    expr = constants.EX_IS
    default_pattern = '{0} IS {1}'


class IsNot(Expression):
    expr = constants.EX_NOT_IS
    default_pattern = '{0} IS NOT {1}'


class In(Expression):
    expr = constants.EX_IN
    default_pattern = '{0} IN {1}'

    def __init__(self, value1, value2):
        value2 = tuple(value2) if isinstance(value2, list) else value2
        Expression.__init__(self, value1, value2)


class NotIn(In):
    expr = constants.EX_NOT_IN
    default_pattern = '{0} NOT IN {1}'


class Like(Expression):
    expr = constants.EX_LIKE
    default_pattern = '{0} LIKE {1}'


class NotLike(Expression):
    expr = constants.EX_NOT_ILIKE
    default_pattern = '{0} NOT LIKE {1}'


class ILike(Expression):
    expr = constants.EX_ILIKE
    default_pattern = '{0} ILIKE {1}'


class NotILike(Expression):
    expr = constants.EX_ILIKE
    default_pattern = '{0} NOT ILIKE {1}'


class Between(Expression):
    expr = constants.EX_BETWEEN
    default_pattern = '{0} BETWEEN {1} AND {2}'

    def __init__(self, value1, value2, value3):
        Expression.__init__(self, value1, value2)
        self.value3 = value3

    def _get_values(self):
        return self.value1, self.value2, self.value3


class NotBetween(Between):
    expr = constants.EX_NOT_BETWEEN
    default_pattern = '{0} NOT BETWEEN {1} AND {2}'


class Add(Expression):
    expr = constants.MATH_ADD
    default_pattern = '{0} + {1}'


class Sub(Expression):
    expr = constants.MATH_SUB
    default_pattern = '{0} - {1}'


class Mul(Expression):
    expr = constants.MATH_MUL
    default_pattern = '{0} * {1}'


class Div(Expression):
    expr = constants.MATH_DIV
    default_pattern = '{0} / {1}'


class Concat(Expression):
    expr = constants.CONCAT
    default_pattern = '{0} || {1}'


class ConcatMany(Expression):
    expr = constants.CONCAT_MANY
    default_pattern = 'concat({0})'

    def __init__(self, *values):
        self.values = values

    def compile(self, compiler):
        sql, params = compile_iterable(self.values, compiler)
        sql_pattern = self._get_expression_pattern(compiler)
        return sql_pattern.format(', '.join(sql)), params


class Regex(Expression):
    expr = constants.EX_REGEX
    default_pattern = '{0} ~ {1}'


class iRegex(Expression):
    expr = constants.EX_IREGEX
    default_pattern = '{0} ~* {1}'
