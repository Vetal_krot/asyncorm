from asyncorm.interfaces.node import INode
from asyncorm.expressions.operator import Expression
from asyncorm.expressions.func import SQLFunctionBase


class GeomFromWKB(SQLFunctionBase):
    func_name = 'ST_GeomFromWKB'


class GeomFromText(SQLFunctionBase):
    func_name = 'ST_GeomFromText'


class ContainsCompletely(Expression):
    default_pattern = '{0} ~ {1}'


class ContainsOverlap(Expression):
    default_pattern = '{0} && {1}'


class ContainsProperly(Expression):
    default_pattern = 'ST_ContainsProperly({0}, {1})'


class ContainedCompletely(Expression):
    default_pattern = '{0} @ {1}'


class ContainedOverlap(Expression):
    default_pattern = 'ST_Contains({0}, {1})'


class CoveredBy(Expression):
    default_pattern = 'ST_CoveredBy({0}, {1})'


class Covers(Expression):
    default_pattern = 'ST_Covers({0}, {1})'


class Crosses(Expression):
    default_pattern = 'ST_Crosses({0}, {1})'


class Disjoint(Expression):
    default_pattern = 'ST_Disjoint({0}, {1})'


class Intersects(Expression):
    default_pattern = 'ST_Intersects({0}, {1})'


class Touches(Expression):
    default_pattern = 'ST_Touches({0}, {1})'


class Within(Expression):
    default_pattern = 'ST_Within({0}, {1})'


class Left(Expression):
    default_pattern = '{0} << {1}'


class Right(Expression):
    default_pattern = '{0} >> {1}'


class Overlaps(Expression):
    default_pattern = 'ST_Overlaps({0}, {1})'


class OverlapsLeft(Expression):
    default_pattern = '{0} &< {1}'


class OverlapsRight(Expression):
    default_pattern = '{0} >& {1}'


class OverlapsAbove(Expression):
    default_pattern = '{0} |&> {1}'


class OverlapsBelow(Expression):
    default_pattern = '{0} &<| {1}'


class StrictlyAbove(Expression):
    default_pattern = '{0} |>> {1}'


class StrictlyBelow(Expression):
    default_pattern = '{0} <<| {1}'


class TupleParamsExprBase(Expression):

    def __init__(self, value1, values):
        self.value1 = value1
        self.value2, self.value3 = values

    def _get_values(self):
        return self.value1, self.value2, self.value3


class DistanceGreat(TupleParamsExprBase):
    default_pattern = 'ST_Distance({0}, {1}, True) > {2}'


class DistanceGreatOrEquals(TupleParamsExprBase):
    default_pattern = 'ST_Distance({0}, {1}, True) >= {2}'


class DistanceLess(TupleParamsExprBase):
    default_pattern = 'ST_Distance({0}, {1}, True) < {2}'


class DistanceLessOrEquals(TupleParamsExprBase):
    default_pattern = 'ST_Distance({0}, {1}, True) <= {2}'


class DWithin(TupleParamsExprBase):
    default_pattern = 'ST_DWithin({0}, {1}, {2}, True)'


class Relate(TupleParamsExprBase):
    default_pattern = 'ST_Relate({0}, {1}, {2})'


@INode.register
class GisNode(object):

    def bbcontains(self, other):
        return ContainsCompletely(self, other)

    def bboverlaps(self, other):
        return ContainsOverlap(self, other)

    def contained(self, other):
        return ContainedCompletely(self, other)

    def contains(self, other):
        return ContainedOverlap(self, other)

    def contains_properly(self, other):
        return ContainsProperly(self, other)

    def coveredby(self, other):
        return CoveredBy(self, other)

    def covers(self, other):
        return Covers(self, other)

    def crosses(self, other):
        return Crosses(self, other)

    def disjoint(self, other):
        return Disjoint(self, other)

    def intersects(self, other):
        return Intersects(self, other)

    def relate(self, other):
        return Relate(self, other)

    def overlaps(self, other):
        return Overlaps(self, other)

    def touches(self, other):
        return Touches(self, other)

    def within(self, other):
        return Within(self, other)

    def left(self, other):
        return Left(self, other)

    def right(self, other):
        return Right(self, other)

    def overlaps_left(self, other):
        return OverlapsLeft(self, other)

    def overlaps_right(self, other):
        return OverlapsRight(self, other)

    def overlaps_above(self, other):
        return OverlapsAbove(self, other)

    def overlaps_below(self, other):
        return OverlapsBelow(self, other)

    def strictly_above(self, other):
        return StrictlyAbove(self, other)

    def strictly_below(self, other):
        return StrictlyBelow(self, other)

    def dwithin(self, other):
        return DWithin(self, other)

    def distance_gt(self, other):
        return DistanceGreat(self, other)

    def distance_gte(self, other):
        return DistanceGreatOrEquals(self, other)

    def distance_lt(self, other):
        return DistanceLess(self, other)

    def distance_lte(self, other):
        return DistanceLessOrEquals(self, other)
