from asyncorm.interfaces.node import INode
from asyncorm.expressions.structure import JoinExpressions
from asyncorm.expressions.operator import (Expression, Equals, NotEquals,
    Less, LessOrEquals, Great, GreatOrEquals, Concat)


__all__ = ['ARRAY']


@INode.register
class ArrayNode(object):

    def equals(self, other):
        return Equals(self, other)

    def not_equals(self, other):
        return NotEquals(self, other)

    def less(self, other):
        return Less(self, other)

    def less_or_equals(self, other):
        return LessOrEquals(self, other)

    def great(self, other):
        return Great(self, other)

    def great_or_equals(self, other):
        return GreatOrEquals(self, other)

    def contains(self, other):
        return Contains(self, other)

    def contained(self, other):
        return Contained(self, other)

    def concat(self, other):
        return Concat(self, other)

    __eq__ = eq = equals
    __ne__ = ne = not_equals
    __lt__ = lt = less
    __le__ = lte = less_or_equals
    __gt__ = gt = great
    __ge__ = gte = great_or_equals
    __add__ = add = concat


class ARRAY(ArrayNode, JoinExpressions):
    delimiter = ', '

    def __init__(self, *values):
        self.values = values

    def compile(self, compiler):
        sql, params = JoinExpressions.compile(self, compiler)
        return 'ARRAY[{0}]'.format(sql), params


class Contains(Expression):
    default_pattern = '{0} @> {1}'


class Contained(Expression):
    default_pattern = '{0} <@ {1}'
