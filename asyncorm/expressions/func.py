from asyncorm.interfaces.model import IModel
from asyncorm.interfaces.compiler import ICompilable
from asyncorm.interfaces.field import IField
from asyncorm.expressions.operator import Node
from asyncorm.expressions.util import compile_iterable


__all__ = ['COUNT', 'SUM', 'MIN', 'MAX', 'AVG', 'Replace', 'Lower', 'Upper']

@ICompilable.register
class SQLFunctionBase(Node):
    func_name = None

    def __init__(self, *params):
        self.params = params

    def get_params(self):
        return self.params

    def get_func_name(self):
        return self.func_name or self.__class__.__name__.lower()

    def compile(self, compiler):
        sql, params = compile_iterable(self.get_params(), compiler)
        if isinstance(sql, list):
            sql = ', '.join(sql)
        return '{0}({1})'.format(self.get_func_name(), sql), params


@ICompilable.register
class AggregateSQLFuncBase(object):
    sql_func = ''

    def __init__(self, field):
        self.field = field

    def get_args(self):
        if isinstance(self.field, IField):
            return self.field.get_sqlname()
        return self.field

    def compile(self, compiler):
        return '{0}({1})'.format(self.sql_func, self.get_args()), None


class COUNT(AggregateSQLFuncBase):
    sql_func = 'COUNT'

    def __init__(self, table=None, field=None):
        super().__init__(field)
        self.table = table

    def get_args(self):
        if isinstance(self.field, IField):
            return self.field.get_sqlname()
        if isinstance(self.table, IModel):
            return self.table.pk.get_sqlname()
        field = self.field or '*'
        if self.table:
            return '"{0}".{1}'.format(self.table, field)
        return field


class MAX(AggregateSQLFuncBase):
    sql_func = 'MAX'


class MIN(AggregateSQLFuncBase):
    sql_func = 'MIN'


class SUM(AggregateSQLFuncBase):
    sql_func = 'SUM'


class AVG(AggregateSQLFuncBase):
    sql_func = 'AVG'


class Replace(SQLFunctionBase):
    pass


class Lower(SQLFunctionBase):
    pass


class Upper(SQLFunctionBase):
    pass
