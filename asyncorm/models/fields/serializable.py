import json
from asyncorm.models.fields.strings import TextField


__all__ = ['JSONField']


class JSONField(TextField):

    def encoder(self, value):
        return json.dumps(value)

    def decoder(self, value):
        return json.loads(value) if value else {}