import abc


class IModelOptions(abc.ABC):
    """
    Container for data specify with model
    """
    name = NotImplemented
    db_table = NotImplemented
    fields = NotImplemented
    relations = NotImplemented
    pk_field = NotImplemented

    @abc.abstractmethod
    def get_tablename(self):
        """
        Get name of table for model
        """

    @abc.abstractmethod
    def iterfields(self, exclude_pk=False):
        """
        Get fields of model in iterator

        :param exclude_pk: Exclude foreign keys from result
        """


class IModel(abc.ABC):
    """
    Python abstraction over db model
    """
    _meta = NotImplemented
    pk = NotImplemented
    manager = NotImplemented

    @abc.abstractclassmethod
    def get_meta(cls):
        """
        Get for self._meta
        """

    @abc.abstractmethod
    def save(self):
        """
        Save values to model

        :return: defer.Deferred
        """

    @abc.abstractmethod
    def delete(self):
        """
        Delete data from db

        :return: defer.Deferred
        """
