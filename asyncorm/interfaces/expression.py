import abc


class IExpression(abc.ABC):
    """
    Base node functionality
    """

    @abc.abstractmethod
    def equals(self, other):
        """
        Check if this node
        :param other:
        :return:
        """

