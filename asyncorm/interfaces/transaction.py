import abc


class ITransaction(abc.ABC):

    @abc.abstractmethod
    def db_insert(self, sql, params):
        """
        Execute sql insert query

        :param sql: query
        :type sql: str
        :param params: params for query
        :type params: list
        :return: last insert id
        :rtype: defer.Deferred
        """

    @abc.abstractmethod
    def db_select(self, sql, params):
        """
        Execute sql select query

        :param sql: query
        :type sql: str
        :param params: params for query
        :type params: list
        :return: result of query
        :rtype: defer.Deferred
        """

    @abc.abstractmethod
    def db_update(self, sql, params):
        """
        Execute sql update query

        :param sql: query
        :type sql: str
        :param params: params for query
        :type params: list
        :return: result of query
        :rtype: defer.Deferred
        """

    @abc.abstractmethod
    def db_delete(self, sql, params):
        """
        Execute sql delete query

        :param sql: query
        :type sql: str
        :param params: params for query
        :type params: list
        :return: result of query
        :rtype: defer.Deferred
        """

    @abc.abstractmethod
    def get_compiler(self):
        """
        Get sql compiler specific for using db

        :return: implementer of `ICompiler`
        :rtype: object
        """

    @abc.abstractmethod
    def get_features(self):
        """
        Get features specific for using db

        :rtype: object
        """

    @abc.abstractmethod
    def db_transaction(self, func, *args, **kwargs):
        """
        Run func with transaction

        :param func: target function handler
        :param args: args for function handler
        :param kwargs: kwargs for function handler
        :return: coroutine
        """
