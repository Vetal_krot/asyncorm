import abc


class IRelation(abc.ABC):
    """
    Relation between models
    """


class IOnoToOnoRelation(IRelation):
    pass


class IOnoToManyRelation(IRelation):
    pass


class IManyToOneRelation(IRelation):
    pass


class IManyToManyRelation(IRelation):
    pass


class IRelatedField(IRelation):
    """
    Field relation for model
    """


class IManyToManyField(IRelation):
    """
    Many to many field relation
    """